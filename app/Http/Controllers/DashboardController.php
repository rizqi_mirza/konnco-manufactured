<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    //
    public function index(){
        $userID = Auth::user()->user_id;
        // $userID = Session::get('userId');

        return view('admin.dashboard', compact('userID'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    public function index(){
        $userID = Auth::user()->user_id;
        $datas = Product::where('product_user_id',$userID)->get();
        // dd($datas);
        return view('admin.products.index', compact('datas'));
    }

    public function store(Request $request){
        $this->validate($request,[
            "name_product" => 'required',
            "price_product" => 'required',
            "status" => 'required'
        ]);

        $product = new Product;
        $product->product_user_id = Session::get('userId');
        $product->name_product = $request->name_product;
        $product->price = $request->price_product;
        $product->status = $request->status;
        $payload = $product->save();
        return redirect('/product');
        // dd("true");
    }

    public function edit($id){
        $data = Product::where('id_product',$id)->first();
        return view('admin.products.edit-product', compact('data'));
    }

    public function update(Request $request,$id){
        $this->validate($request,[
            "name_product" => 'required',
            "price_product" => 'required',
            "status" => 'required'
        ]);

        $update = array(
            'product_user_id' => Session::get('userId'),
            'name_product' => $request->name_product,
            'price' => $request->price_product,
            'status' => $request->status,
        );
        
        Product::where('id_product',$id)->update($update);
        return redirect('/product');

    }

    public function destroy($id){
        $data = Product::findOrFail($id);

        Product::where('id_product',$id)->delete();
        return redirect('/product');

    }
}

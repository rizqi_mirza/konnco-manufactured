<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function register(Request $request){
        $this->validate($request,[
            "name"      => "required",
            "email"     => "required",
            "password"  => "required",

        ]);

        $payload = new User;
        $payload->name =  $request->name;
        $payload->email = $request->email;
        $payload->password = Hash::make($request->password);
        $result = $payload->save();
        // dd($result);
        
        // if($result){
        //     $request->session()->flash("Registrasi Berhasil");
            return redirect("/login");
        // }
        // $request->session();

    }
}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('registrasi');
});
Route::post('/register', 'UserController@register');

Route::get('/login', function () {
    return view('login');
});
Route::post('/authenticate', 'AuthController@authenticate');
Route::get('/logout', 'AuthController@logout');
Route::get('/dashboard', 'DashboardController@index');

Route::get('/product', 'ProductController@index');


Route::get('/create-product', function () {
    return view('admin/products/create-product');
});
Route::post('/post-product', 'ProductController@store');

Route::get('/edit-product/{id}', 'ProductController@edit');
Route::post('/update-product/{id}', 'ProductController@update');

Route::get('/delete-product/{id}', 'ProductController@destroy');
